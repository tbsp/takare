# Takare

A personal virtual computer debugger based on the [Uxn stack-machine](https://wiki.xxiivv.com/site/uxn.html), written in ANSI C.

This is a fork of the reference ANSI C Uxn emulator, converted to a Takalaki emulator, and wrapped in a dear imgui frontend with added debugging capabilities.

# Issues

- Debugger UI doesn't redraw when clicking/pressing step buttons until the mouse moves to trigger a redraw
- Breakpoints in certain vector handlers (ie; Raster/vector) won't resume properly

## License
This library is free software; you can redistribute it and/or modify it under
the terms of the MIT license. See [LICENSE](LICENSE) for details.

