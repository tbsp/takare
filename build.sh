#!/bin/sh -e

format=0
console=0
install=0
debug=0
norun=0

while [ $# -gt 0 ]; do
	case $1 in
		--format)
			format=1
			shift
			;;

		--console)
			console=1
			shift
			;;

		--install)
			install=1
			shift
			;;

		--debug)
			debug=1
			shift
			;;

		--no-run)
			norun=1
			shift
			;;

		*)
			shift
	esac
done

# When clang-format is present

if [ $format = 1 ];
then
	echo "Formatting.."
	clang-format -i src/devices/system.h
	clang-format -i src/devices/system.c
	clang-format -i src/devices/console.h
	clang-format -i src/devices/console.c
	clang-format -i src/devices/screen.h
	clang-format -i src/devices/screen.c
	clang-format -i src/devices/raster.h
	clang-format -i src/devices/raster.c
	clang-format -i src/devices/audio.h
	clang-format -i src/devices/audio.c
	clang-format -i src/devices/file.h
	clang-format -i src/devices/file.c
	clang-format -i src/devices/mouse.h
	clang-format -i src/devices/mouse.c
	clang-format -i src/devices/controller.h
	clang-format -i src/devices/controller.c
	clang-format -i src/devices/datetime.h
	clang-format -i src/devices/datetime.c
	clang-format -i src/main.c
	clang-format -i src/debugger.c
fi

mkdir -p bin
CC="${CC:-g++}"
#CFLAGS="${CFLAGS}"
case "$(uname -s 2>/dev/null)" in
MSYS_NT*|MINGW*) # MSYS2 on Windows
	FILE_LDFLAGS="-liberty"
	if [ $console = 1 ];
	then
		TAKABUG_LDFLAGS="-static $(sdl2-config --cflags --static-libs | sed -e 's/ -mwindows//g')"
	else
		TAKABUG_LDFLAGS="-static $(sdl2-config --cflags --static-libs)"
	fi
	;;
Darwin) # macOS
	CFLAGS="${CFLAGS} -Wno-typedef-redefinition"
	TAKABUG_LDFLAGS="$(brew --prefix)/lib/libSDL2.a $(sdl2-config --cflags --static-libs | sed -e 's/-lSDL2 //')"
	;;
Linux|*)
	TAKABUG_LDFLAGS="-L/usr/local/lib $(sdl2-config --cflags --libs)"
	;;
esac

TP_DIR="third_party"
IMGUI_DIR="third_party/imgui"

if [ $debug = 1 ];
then
	echo "[debug]"
	CFLAGS="${CFLAGS} -I${TP_DIR} -I${IMGUI_DIR} -I${IMGUI_DIR}/backends -fpermissive -DDEBUG -Wpedantic -Wshadow -Wextra -Wno-missing-field-initializers -Wvla -g -Og -fsanitize=address -fsanitize=undefined"
else
	CFLAGS="${CFLAGS} -I${TP_DIR} -I${IMGUI_DIR} -I${IMGUI_DIR}/backends -fpermissive -DNDEBUG -O2 -g0 -s"
fi

SOURCES="src/laki.c"
SOURCES="${SOURCES} src/devices/system.c src/devices/console.c src/devices/file.c src/devices/datetime.c src/devices/mmu.c src/devices/mouse.c src/devices/controller.c src/devices/screen.c src/devices/raster.c src/devices/audio.c"
SOURCES="${SOURCES} src/main.c src/debugger/debugger.cc"
SOURCES="${SOURCES} ${IMGUI_DIR}/imgui.cpp ${IMGUI_DIR}/imgui_demo.cpp ${IMGUI_DIR}/imgui_draw.cpp ${IMGUI_DIR}/imgui_tables.cpp ${IMGUI_DIR}/imgui_widgets.cpp"
SOURCES="${SOURCES} ${IMGUI_DIR}/backends/imgui_impl_sdl2.cpp ${IMGUI_DIR}/backends/imgui_impl_sdlrenderer2.cpp"

echo "Building.."
${CC} ${CFLAGS} ${SOURCES} ${TAKABUG_LDFLAGS} ${FILE_LDFLAGS} -o takui
