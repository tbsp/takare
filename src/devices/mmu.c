#include "../laki.h"
#include "mmu.h"

/*
Copyright (c) 2024 Dave VanEe

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE.
*/

/* IO */

void
mmu_sev(Uint8 *ram, Uint8 *d, Uint8 port)
{
    Uint16 i, j;
	switch(port) {
	case 0xe: {
        Uint16 dst = PEEK2(d + 0x2), dstOffset = PEEK2(d + 0x6), length = PEEK2(d + 0xc);
        Uint8 count = (d[0xe] & 0x7f) + 1, cmd = d[0xe] & 0x80;
        if(!cmd) { /* set */
            Uint8 value = d[0x0];
            for(j = 0; j < count; j++) {
			    for(i = 0; i < length; i++)
    				ram[dst + i] = value;
                dst += dstOffset;
            }
        } else { /* copy */
            Uint16 src = PEEK2(d), srcOffset = PEEK2(d + 0x4);
            Uint8 bank = d[0xf];
			int src_base = (bank % RAM_PAGES) * 0x10000;
            if(!bank && src < dst) {
                /* reverse copy */
                src += (length + srcOffset) * (count - 1);
                dst += (length + dstOffset) * (count - 1);
                for(j = count - 1; j != 0xffff; j--) {
                    for(i = length - 1; i != 0xffff; i--) {
                        ram[dst + i] = ram[src_base + (src + i)];
                    }
                    src -= (length + srcOffset);
                    dst -= (length + dstOffset);
                }
            } else {
                /* forward copy */
                for(j = 0; j < count; j++) {
                    for(i = 0; i < length; i++) {
                        ram[dst + i] = ram[src_base + (src + i)];
                    }
                    src += (length + srcOffset);
                    dst += (length + dstOffset);
                }
            }
        }
		break;
	}
	}
}
