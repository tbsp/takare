/*
Copyright (c) 2024 Dave VanEe

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE.
*/

#define RAM_PAGES 0x10

void mmu_sev(Uint8 *ram, Uint8 *d, Uint8 port);