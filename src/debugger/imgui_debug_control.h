// Debug Control for Dear ImGui (to embed in your game/tools)
//
// Usage:
//   // Create a window and draw stack viewer inside it:
//   static DebugControl debug_control_1;
//   static char data[0x10000];
//   debug_control_1.DrawWindow("Debug Control", u);
//
// Usage:
//   // If you already have a window, use DrawContents() instead:
//   static DebugControl debug_control_2;
//   ImGui::Begin("MyWindow")
//   debug_control_2.DrawContents(this, sizeof(*this), (size_t)this);
//   ImGui::End();
//
// Changelog:
// - v0.10: initial version
//
// Todo/Bugs:
//

#pragma once

#include <stdio.h>      // sprintf, scanf
#include <stdint.h>     // uint8_t, etc.
#include "../laki.h"

#ifdef _MSC_VER
#define _PRISizeT   "I"
#define ImSnprintf  _snprintf
#else
#define _PRISizeT   "z"
#define ImSnprintf  snprintf
#endif

#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable: 4996) // warning C4996: 'sprintf': This function or variable may be unsafe.
#endif

struct DebugControl
{
    // Settings
    bool            Open;                                       // = true   // set to false when DrawWindow() was closed. ignore if not using DrawWindow().

    // [Internal State]
    DebugControl()
    {
        // Settings
        Open = true;
    }

    // Standalone Debug Control window
    void DrawWindow(const char* title, Laki* u)
    {
        Open = true;
        if (ImGui::Begin(title, &Open, ImGuiWindowFlags_NoScrollbar))
        {
            if (ImGui::IsWindowHovered(ImGuiHoveredFlags_RootAndChildWindows) && ImGui::IsMouseReleased(ImGuiMouseButton_Right))
                ImGui::OpenPopup("context");
            DrawContents(u);
        }
        ImGui::End();
    }

    // Debug Control contents only
    void DrawContents(Laki* u)
    {
        ImGui::Text("PC: %04x", u->pc);
        ImGui::SameLine();
        if(!u->running) {
            if(ImGui::Button("Run (F5)")) u->running = true;
        } else {
            ImGui::BeginDisabled();
            ImGui::Button("Run (F5)");
            ImGui::EndDisabled();
        }
        ImGui::SameLine();
        if(u->vector_active) {
            if(ImGui::Button("Step (F6)")) run_laki(u, 1, false, true);
        } else {
            ImGui::BeginDisabled();
            ImGui::Button("Step (F6)");
            ImGui::EndDisabled();
        }
    }
};

#undef _PRISizeT
#undef ImSnprintf

#ifdef _MSC_VER
#pragma warning (pop)
#endif
