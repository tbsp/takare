#include <stdio.h>
#include <SDL2/SDL.h>
#include <assert.h>
#include "debugger.h"

static int width  = 1024;
static int height = 768;

static SDL_Window *dbgWindow;
static SDL_Renderer *dbgRenderer;

ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

void debugger_init(void) {
  dbgWindow = SDL_CreateWindow(
    NULL, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
    width, height, SDL_WINDOW_SHOWN);
  if(dbgWindow == NULL)
    return;
  dbgRenderer = SDL_CreateRenderer(dbgWindow, -1, 0);
  if(dbgRenderer == NULL)
    return;

	/* init imgui */
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;
	ImGui::StyleColorsDark();
	ImGui_ImplSDL2_InitForSDLRenderer(dbgWindow, dbgRenderer);
	ImGui_ImplSDLRenderer2_Init(dbgRenderer);
}

void debugger_process_frame(Laki *u) {

  ImGui_ImplSDLRenderer2_NewFrame();
  ImGui_ImplSDL2_NewFrame();
  ImGui::NewFrame();

  ImGui::SetNextWindowPos(ImVec2(0, 0));
  ImGui::SetNextWindowSize(ImVec2(264, 768));
  ImGui::Begin("Program", nullptr, ImGuiWindowFlags_NoScrollbar);
  static ProgramEditor prg_edit;
  size_t prg_size = 0x10000;
  prg_edit.DrawContents(u->ram, prg_size, 0, u->pc);
  ImGui::End();

  ImGui::SetNextWindowPos(ImVec2(263, 0));
  ImGui::SetNextWindowSize(ImVec2(105, 768));
  ImGui::Begin("WST", nullptr, ImGuiWindowFlags_NoScrollbar);
  static StackViewer stack_wst;
  size_t wst_size = 0x100;
  stack_wst.DrawContents(u->wst.dat, wst_size, u->wst.ptr);
  ImGui::End();

  ImGui::SetNextWindowPos(ImVec2(367, 0));
  ImGui::SetNextWindowSize(ImVec2(105, 768));
  ImGui::Begin("RST", nullptr, ImGuiWindowFlags_NoScrollbar);
  static StackViewer stack_rst;
  size_t rst_size = 0x100;
  stack_rst.DrawContents(u->rst.dat, rst_size, u->rst.ptr);
  ImGui::End();

  ImGui::SetNextWindowPos(ImVec2(471, 0));
  ImGui::SetNextWindowSize(ImVec2(555, 52));
  ImGui::Begin("Debug Control", nullptr, ImGuiWindowFlags_NoScrollbar);
  static DebugControl debug_control;
  debug_control.DrawContents(u);
  ImGui::End();

  ImGui::SetNextWindowPos(ImVec2(471, 52));
  ImGui::SetNextWindowSize(ImVec2(555, 446));
  ImGui::Begin("Laki RAM", nullptr, ImGuiWindowFlags_NoScrollbar);
  static MemoryEditor mem_edit_ram;
  size_t ram_size = 0x10000;
  mem_edit_ram.DrawContents(u->ram, ram_size);
  ImGui::End();

  ImGui::SetNextWindowPos(ImVec2(471, 498));
  ImGui::SetNextWindowSize(ImVec2(555, 270));
  ImGui::Begin("Device RAM", nullptr, ImGuiWindowFlags_NoScrollbar);
  static MemoryEditor mem_edit_dev;
  size_t dev_size = 0x100;
  mem_edit_dev.DrawContents(u->dev, dev_size);
  ImGui::End();

  ImGui::Render();
  ImGuiIO& io = ImGui::GetIO(); (void)io;
  SDL_RenderSetScale(dbgRenderer, io.DisplayFramebufferScale.x, io.DisplayFramebufferScale.y);
  SDL_SetRenderDrawColor(dbgRenderer, (Uint8)(clear_color.x * 255), (Uint8)(clear_color.y * 255), (Uint8)(clear_color.z * 255), (Uint8)(clear_color.w * 255));
  SDL_RenderClear(dbgRenderer);
  ImGui_ImplSDLRenderer2_RenderDrawData(ImGui::GetDrawData());
  SDL_RenderPresent(dbgRenderer);
}

void debugger_shutdown(void) {
  ImGui_ImplSDLRenderer2_Shutdown();
	ImGui_ImplSDL2_Shutdown();
	ImGui::DestroyContext();

  SDL_DestroyRenderer(dbgRenderer);
  SDL_DestroyWindow(dbgWindow);
}
