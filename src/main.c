#include <stdio.h>
#include <time.h>
#include <unistd.h>

#include "laki.h"
#include "debugger/debugger.h"

#pragma GCC diagnostic push
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#pragma clang diagnostic ignored "-Wtypedef-redefinition"
#include <SDL2/SDL.h>
#include "devices/system.h"
#include "devices/console.h"
#include "devices/screen.h"
#include "devices/raster.h"
#include "devices/audio.h"
#include "devices/file.h"
#include "devices/controller.h"
#include "devices/mouse.h"
#include "devices/datetime.h"
#include "devices/mmu.h"
#if defined(_WIN32) && defined(_WIN32_WINNT) && _WIN32_WINNT > 0x0602
#include <processthreadsapi.h>
#elif defined(_WIN32)
#include <windows.h>
#include <string.h>
#endif
#ifndef __plan9__
#define USED(x) (void)(x)
#endif
#pragma GCC diagnostic pop
#pragma clang diagnostic pop

/*
Copyright (c) 2021-2023 Devine Lu Linvega, Andrew Alderwick
Copyright (c) 2023 Dave VanEe

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE.
*/

#define WIDTH 64 * 8
#define HEIGHT 40 * 8
#define PAD 2
#define PAD2 4
#define TIMEOUT_MS 334
#define BENCH 0

static SDL_Window *emu_window;
static SDL_Texture *emu_texture;
static SDL_Renderer *emu_renderer;
static SDL_AudioDeviceID audio_id;
static SDL_Rect emu_viewport;
static SDL_Thread *stdin_thread;

static float bg[3] = { 90, 95, 100 };

Uint16 deo_mask[] = {0xff08, 0x0300, 0xc028, 0x8000, 0x8000, 0x8000, 0x8000, 0x0000, 0x0000, 0x0000, 0xa260, 0xa260, 0x0000, 0x0000, 0xffff, 0x0000};
Uint16 dei_mask[] = {0x0000, 0x0000, 0x003c, 0x0014, 0x0014, 0x0014, 0x0014, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x07ff, 0x0000, 0x0000, 0x0000};

/* devices */

static int window_created;
static Uint8 zoom = 1, uxn_has_focus = 1;
static Uint32 stdin_event, audio0_event, uxn_window_ID;
static Uint64 exec_deadline, deadline_interval, ms_interval;

static int
clamp(int v, int min, int max)
{
	return v < min ? min : v > max ? max
								   : v;
}

char *rom_path;

static int
error(const char *msg, const char *err)
{
	fprintf(stderr, "%s: %s\n", msg, err);
	fflush(stderr);
	return 0;
}

static int
console_input(Laki *u, char c)
{
	Uint8 *d = &u->dev[0x10];
	d[0x02] = c;
	return run_vector(u, PEEK2(d), true);
}

static void
audio_sev(int instance, Uint8 *d, Uint8 port, Laki *u)
{
	if(!audio_id) return;
	if(port == 0xf) {
		SDL_LockAudioDevice(audio_id);
		audio_start(instance, d, u);
		SDL_UnlockAudioDevice(audio_id);
		SDL_PauseAudioDevice(audio_id, 0);
	}
}

Uint8
emu_lev(Laki *u, Uint8 addr)
{
	Uint8 p = addr & 0x0f, d = addr & 0xf0;
	switch(d) {
	case 0x20: return screen_lev(u, addr);
	case 0x40: return audio_lev(0, &u->dev[d], p);
	case 0x50: return audio_lev(1, &u->dev[d], p);
	case 0x60: return audio_lev(2, &u->dev[d], p);
	case 0x70: return audio_lev(3, &u->dev[d], p);
	case 0xc0: return datetime_lev(u, addr);
	}
	return u->dev[addr];
}

void
emu_sev(Laki *u, Uint8 addr, Uint8 value)
{
	Uint8 p = addr & 0x0f, d = addr & 0xf0;
	u->dev[addr] = value;
	switch(d) {
	case 0x00: system_sev(u, &u->dev[d], p); break;
	case 0x10: console_sev(&u->dev[d], p); break;
	case 0x20: screen_sev(u->ram, &u->dev[0x20], p); break;
	case 0x30: raster_sev(u->ram, &u->dev[d], p); break;
	case 0x40: audio_sev(0, &u->dev[d], p, u); break;
	case 0x50: audio_sev(1, &u->dev[d], p, u); break;
	case 0x60: audio_sev(2, &u->dev[d], p, u); break;
	case 0x70: audio_sev(3, &u->dev[d], p, u); break;
	case 0xa0: file_sev(0, u->ram, &u->dev[d], p); break;
	case 0xb0: file_sev(1, u->ram, &u->dev[d], p); break;
	case 0xd0: mmu_sev(u->ram, &u->dev[d], p); break;
	}
}

#pragma mark - Generics

static void
audio_callback(void *u, Uint8 *stream, int len)
{
	int instance, running = 0;
	Sint16 *samples = (Sint16 *)stream;
	USED(u);
	SDL_memset(stream, 0, len);
	for(instance = 0; instance < POLYPHONY; instance++)
		running += audio_render(instance, samples, samples + len / 2);
	if(!running)
		SDL_PauseAudioDevice(audio_id, 1);
}

void
audio_finished_handler(int instance)
{
	SDL_Event event;
	event.type = audio0_event + instance;
	SDL_PushEvent(&event);
}

static int
stdin_handler(void *p)
{
	SDL_Event event;
	USED(p);
	event.type = stdin_event;
	while(read(0, &event.cbutton.button, 1) > 0 && SDL_PushEvent(&event) >= 0)
		;
	return 0;
}

static void
set_window_size(SDL_Window *window, int w, int h)
{
	SDL_Point win, win_old;
	SDL_GetWindowPosition(window, &win.x, &win.y);
	SDL_GetWindowSize(window, &win_old.x, &win_old.y);
	if(w == win_old.x && h == win_old.y) return;
	SDL_SetWindowPosition(window, (win.x + win_old.x / 2) - w / 2, (win.y + win_old.y / 2) - h / 2);
	SDL_SetWindowSize(window, w, h);
}

int
emu_resize(int width, int height)
{
	if(!window_created)
		return 0;
	if(emu_texture != NULL)
		SDL_DestroyTexture(emu_texture);
	SDL_RenderSetLogicalSize(emu_renderer, width + PAD2, height + PAD2);
	emu_texture = SDL_CreateTexture(emu_renderer, SDL_PIXELFORMAT_RGB888, SDL_TEXTUREACCESS_STATIC, width, height);
	if(emu_texture == NULL || SDL_SetTextureBlendMode(emu_texture, SDL_BLENDMODE_NONE))
		return system_error("SDL_SetTextureBlendMode", SDL_GetError());
	if(SDL_UpdateTexture(emu_texture, NULL, taka_screen.pixels, sizeof(Uint32)) != 0)
		return system_error("SDL_UpdateTexture", SDL_GetError());
	emu_viewport.x = PAD;
	emu_viewport.y = PAD;
	emu_viewport.w = taka_screen.width;
	emu_viewport.h = taka_screen.height;
	set_window_size(emu_window, (width + PAD2) * zoom, (height + PAD2) * zoom);
	return 1;
}

static void
emu_redraw(Laki *u)
{
	if(emu_viewport.w != taka_screen.width || emu_viewport.h != taka_screen.height) emu_resize(taka_screen.width, taka_screen.height);
	screen_redraw(u);
	if(SDL_UpdateTexture(emu_texture, NULL, taka_screen.pixels, taka_screen.width * sizeof(Uint32)) != 0)
		error("SDL_UpdateTexture", SDL_GetError());
	SDL_RenderClear(emu_renderer);
	SDL_RenderCopy(emu_renderer, emu_texture, NULL, &emu_viewport);
	SDL_RenderPresent(emu_renderer);
}

static int
emu_init(Laki *u)
{
	SDL_AudioSpec as;
	SDL_zero(as);
	as.freq = SAMPLE_FREQUENCY;
	as.format = AUDIO_S16SYS;
	as.channels = 2;
	as.callback = audio_handler;
	as.samples = AUDIO_BUFSIZE;
	as.userdata = NULL; //u;
	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_JOYSTICK) < 0)
		return system_error("sdl", SDL_GetError());
	audio_id = SDL_OpenAudioDevice(NULL, 0, &as, NULL, 0);
	if(!audio_id)
		system_error("sdl_audio", SDL_GetError());
	if(SDL_NumJoysticks() > 0 && SDL_JoystickOpen(0) == NULL)
		system_error("sdl_joystick", SDL_GetError());
	stdin_event = SDL_RegisterEvents(1);
	audio0_event = SDL_RegisterEvents(POLYPHONY);
	SDL_DetachThread(stdin_thread = SDL_CreateThread(stdin_handler, "stdin", NULL));
	SDL_StartTextInput();
	SDL_ShowCursor(SDL_DISABLE);
	SDL_EventState(SDL_DROPFILE, SDL_ENABLE);
	SDL_SetRenderDrawColor(emu_renderer, 0x00, 0x00, 0x00, 0xff);
	ms_interval = SDL_GetPerformanceFrequency() / 1000;
	deadline_interval = ms_interval * TIMEOUT_MS;
	exec_deadline = SDL_GetPerformanceCounter() + deadline_interval;
	screen_resize(WIDTH, HEIGHT);
	SDL_PauseAudioDevice(audio_id, 1);
	debugger_init();
	return 1;
}

#pragma mark - Devices

/* Boot */

static void
set_zoom(Uint8 z, int win)
{
	if(z < 1) return;
	if(win)
		set_window_size(emu_window, (taka_screen.width + PAD2) * z, (taka_screen.height + PAD2) * z);
	zoom = z;
}

static void
capture_screen(void)
{
	const Uint32 format = SDL_PIXELFORMAT_RGB24;
	time_t t = time(NULL);
	char fname[64];
	int w, h;
	SDL_Surface *surface;
	SDL_GetRendererOutputSize(emu_renderer, &w, &h);
	surface = SDL_CreateRGBSurfaceWithFormat(0, w, h, 24, format);
	SDL_RenderReadPixels(emu_renderer, NULL, format, surface->pixels, surface->pitch);
	strftime(fname, sizeof(fname), "screenshot-%Y%m%d-%H%M%S.bmp", localtime(&t));
	SDL_SaveBMP(surface, fname);
	SDL_FreeSurface(surface);
	fprintf(stderr, "Saved %s\n", fname);
	fflush(stderr);
}

static void
emu_restart(Laki *u, char *rom, int soft)
{
	screen_resize(WIDTH, HEIGHT);
	screen_fill(taka_screen.bg, 0);
	screen_fill(taka_screen.fg, 0);
	system_reboot(u, rom, soft);
	SDL_SetWindowTitle(emu_window, boot_rom);
}

static Uint8
get_button(SDL_Event *event)
{
	switch(event->key.keysym.sym) {
	case SDLK_LCTRL: return 0x01;
	case SDLK_LALT: return 0x02;
	case SDLK_LSHIFT: return 0x04;
	case SDLK_HOME: return 0x08;
	case SDLK_UP: return 0x10;
	case SDLK_DOWN: return 0x20;
	case SDLK_LEFT: return 0x40;
	case SDLK_RIGHT: return 0x80;
	}
	return 0x00;
}

static Uint8
get_button_joystick(SDL_Event *event)
{
	return 0x01 << (event->jbutton.button & 0x3);
}

static Uint8
get_vector_joystick(SDL_Event *event)
{
	if(event->jaxis.value < -3200)
		return 1;
	if(event->jaxis.value > 3200)
		return 2;
	return 0;
}

static Uint8
get_key(SDL_Event *event)
{
	int sym = event->key.keysym.sym;
	SDL_Keymod mods = SDL_GetModState();
	if(sym < 0x20 || sym == SDLK_DELETE)
		return sym;
	if(mods & KMOD_CTRL) {
		if(sym < SDLK_a)
			return sym;
		else if(sym <= SDLK_z)
			return sym - (mods & KMOD_SHIFT) * 0x20;
	}
	switch(sym) {
	case SDLK_INSERT: return 0x10;
	case SDLK_END: return 0x11;
	case SDLK_PAGEUP: return 0x12;
	case SDLK_PAGEDOWN: return 0x13;
	}
	return 0x00;
}

static void
do_shortcut(Laki *u, SDL_Event *event)
{
	if(event->key.keysym.sym == SDLK_F1)
		set_zoom(zoom == 3 ? 1 : zoom + 1, 1);
	/* else if(event->key.keysym.sym == SDLK_F2)
		system_inspect(u); */
	else if(event->key.keysym.sym == SDLK_F3)
		capture_screen();
	else if(event->key.keysym.sym == SDLK_F4)
		emu_restart(u, boot_rom, 0);
	else if(event->key.keysym.sym == SDLK_F5)
		u->running = true;
	else if(event->key.keysym.sym == SDLK_F6 && !u->running && u->vector_active)
		run_laki(u, 1, false, true);
	else if(event->key.keysym.sym == SDLK_F8)
		emu_restart(u, boot_rom, 1);
}

static void
handle_mouse_event(SDL_Event event, Laki *u) {
  /* Mouse */
  if(event.type == SDL_MOUSEMOTION)
	mouse_pos(u, &u->dev[0x90], clamp(event.motion.x - PAD, 0, taka_screen.width - 1), clamp(event.motion.y - PAD, 0, taka_screen.height - 1));
  else if(event.type == SDL_MOUSEBUTTONUP)
	mouse_up(u, &u->dev[0x90], SDL_BUTTON(event.button.button));
  else if(event.type == SDL_MOUSEBUTTONDOWN)
	mouse_down(u, &u->dev[0x90], SDL_BUTTON(event.button.button));
  else if(event.type == SDL_MOUSEWHEEL)
	mouse_scroll(u, &u->dev[0x90], event.wheel.x, event.wheel.y);
}

static void
handle_keyboard_event(SDL_Event event, Laki *u) {
  /* Controller */
  if(!u->running) {
	do_shortcut(u, &event);
  } else {
	if(event.type == SDL_TEXTINPUT)
		controller_key(u, &u->dev[0x80], event.text.text[0]);
	else if(event.type == SDL_KEYDOWN) {
		int ksym;
		if(get_key(&event))
			controller_key(u, &u->dev[0x80], get_key(&event));
		else if(get_button(&event))
			controller_down(u, &u->dev[0x80], get_button(&event));
		else
			do_shortcut(u, &event);
		ksym = event.key.keysym.sym;
		if(SDL_PeepEvents(&event, 1, SDL_PEEKEVENT, SDL_KEYUP, SDL_KEYUP) == 1 && ksym == event.key.keysym.sym) {
			return;
		}
	} else if(event.type == SDL_KEYUP)
		controller_up(u, &u->dev[0x80], get_button(&event));
	else if(event.type == SDL_JOYAXISMOTION) {
		Uint8 vec = get_vector_joystick(&event);
		if(!vec)
			controller_up(u, &u->dev[0x80], (3 << (!event.jaxis.axis * 2)) << 4);
		else
			controller_down(u, &u->dev[0x80], (1 << ((vec + !event.jaxis.axis * 2) - 1)) << 4);
	} else if(event.type == SDL_JOYBUTTONDOWN)
		controller_down(u, &u->dev[0x80], get_button_joystick(&event));
	else if(event.type == SDL_JOYBUTTONUP)
		controller_up(u, &u->dev[0x80], get_button_joystick(&event));
  }
}

static int
handle_events(Laki *u)
{
	SDL_Event event;
	while(SDL_PollEvent(&event)) {
	if(u->running && event.type >= audio0_event && event.type < audio0_event + POLYPHONY)
		run_vector(u, PEEK2(&u->dev[0x30 + 0x10 * (event.type - audio0_event)]), true);
	else if(u->running && event.type == stdin_event)
		console_input(u, event.cbutton.button);
	else {
	  switch(event.type) {
		case SDL_QUIT:
			u->dev[0x0f] = 1;
			return 0;
		case SDL_WINDOWEVENT:
		  switch(event.window.event) {
			case SDL_WINDOWEVENT_CLOSE:
				u->dev[0x0f] = 1;
				return 0;
			case SDL_WINDOWEVENT_EXPOSED:
				if(event.window.windowID == uxn_window_ID) { emu_redraw(u); break; }
			case SDL_WINDOWEVENT_FOCUS_GAINED:
				uxn_has_focus = (event.window.windowID == uxn_window_ID) ? 1 : 0; break;
			case SDL_WINDOWEVENT_ENTER:
				if(event.window.windowID == uxn_window_ID) SDL_ShowCursor(SDL_DISABLE);
				else SDL_ShowCursor(SDL_ENABLE);
		  }
		  break;
		case SDL_DROPFILE:
			screen_resize(WIDTH, HEIGHT);
			emu_restart(u, event.drop.file, 0);
			SDL_free(event.drop.file);
			break;
		default:
			ImGuiIO& io = ImGui::GetIO();
			ImGui_ImplSDL2_ProcessEvent(&event);
			if(u->running && !io.WantCaptureMouse) handle_mouse_event(event, u);
			if(!io.WantCaptureKeyboard) handle_keyboard_event(event, u);
			break;
	  }
	}
	}
	return 1;
}

static int
emu_run(Laki *u)
{
	Uint64 next_refresh = 0;
	Uint64 frame_interval = SDL_GetPerformanceFrequency() / 60;
	Uint8 *vector_addr = &u->dev[0x20], *raster_addr = &u->dev[0x30];
	Uint32 window_flags = SDL_WINDOW_SHOWN | SDL_WINDOW_ALLOW_HIGHDPI;
	emu_window = SDL_CreateWindow("Takalaki",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		(taka_screen.width + PAD2) * zoom,
		(taka_screen.height + PAD2) * zoom,
		window_flags);
	if(emu_window == NULL)
		return system_error("sdl_window", SDL_GetError());
	emu_renderer = SDL_CreateRenderer(emu_window, -1, SDL_RENDERER_ACCELERATED);
	if(emu_renderer == NULL)
		return system_error("sdl_renderer", SDL_GetError());
	window_created = 1;
	emu_resize(taka_screen.width, taka_screen.height);
	for(;;) {
		Uint16 screen_vector, raster_vector;
		Uint64 now = SDL_GetPerformanceCounter();
		bool running;
		/* .System/halt */
		if(u->dev[0x0f])
			return system_error("Run", "Ended.");
		exec_deadline = now + deadline_interval;
		if(!handle_events(u))
			return 0;
		if(u->running && u->vector_active) run_laki(u, 0, false, true); /* Vector cleanup from F5 */
		
		running = u->running;
		if(running) {
			screen_vector = PEEK2(vector_addr);
			raster_vector = PEEK2(raster_addr);
			if(now >= next_refresh) {
				now = SDL_GetPerformanceCounter();
				next_refresh = now + frame_interval;
				run_vector(u, screen_vector, true);
				if(taka_screen.x2 || u->dev[0x26] & 0x1c || raster_vector)
					emu_redraw(u);
			}
		}

		/* Render the debugger UI every frame */
		debugger_process_frame(u);
		if(u->running && u->vector_active) run_laki(u, 0, false, true); /* Vector cleanup from Run button click */

		if(running && (screen_vector || taka_screen.x2 || u->dev[0x26] & 0x1c || raster_vector)) {
			Uint64 delay_ms = (next_refresh - now) / ms_interval;
			if(delay_ms > 0) SDL_Delay(delay_ms);
		} else
			SDL_WaitEvent(NULL);
	}
}

static int
emu_end(Laki *u)
{
	SDL_CloseAudioDevice(audio_id);
#ifdef _WIN32
#pragma GCC diagnostic ignored "-Wint-to-pointer-cast"
	TerminateThread((HANDLE)SDL_GetThreadID(stdin_thread), 0);
#elif !defined(__APPLE__)
	close(0); /* make stdin thread exit */
#endif
	SDL_Quit();
	free(u->ram);
	return u->dev[0x0f] & 0x7f;
}

int
main(int argc, char **argv)
{
	SDL_DisplayMode DM;
	int i = 1;
	Uint8 *ram;
	char *rom;
	Laki u = {0};
	Uint8 dev[0x100] = {0};
	Laki u_audio = {0};
	u.dev = (Uint8 *)&dev;
	u_audio.dev = (Uint8 *)&dev;

	/* flags */
	if(argc > 1 && argv[i][0] == '-') {
		if(!strcmp(argv[i], "-v"))
			return system_error("Takui - Takalaki Debugger(GUI)", "9 February 2024.");
		else if(!strcmp(argv[i], "-2x"))
			set_zoom(2, 0);
		else if(!strcmp(argv[i], "-3x"))
			set_zoom(3, 0);
		/* else if(strcmp(argv[i], "-f") == 0)
			set_fullscreen(1, 0); */
		i++;
	}
	if(i == argc)
		return system_error("usage:", "takui [-v | -f | -2x | -3x] file.aki [args...]");
	/* start */
	ram = (Uint8 *)calloc(0x10000 * RAM_PAGES, sizeof(Uint8));
	rom = argv[i++];

	if(!system_init(&u, ram, rom) || !system_init(&u_audio, ram, rom))
		return system_error("Init", "Failed to initialize laki.");
	if(!emu_init(&u_audio))
		return system_error("Init", "Failed to initialize takabug.");
	/* loop */
	u.dev[0x17] = argc - i;
	if(run_vector(&u, PAGE_PROGRAM, true)) {
		console_listen(&u, i, argc, argv);
		while(!u.dev[0x0f]) {
			emu_run(&u);
			run_vector(&u, PEEK2(&u.dev[0x00]), false);
		}
		error("Run", "Ended.");
	}
	return emu_end(&u);
}
